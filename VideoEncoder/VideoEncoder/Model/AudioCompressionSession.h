//
//  AudioCompressionSession.h
//  VideoEncoder
//
//  Created by Josip Cavar on 23/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreMedia/CoreMedia.h>

@class AudioCompressionSession;


@protocol AudioCompressionSessionDelegate <NSObject>

- (void)compressionSession:(AudioCompressionSession *)session didEncodeSampleBuffer:(void *)sampleBuffer size:(UInt32)size;

@end

@interface AudioCompressionSession : NSObject

@property (weak, nonatomic) id<AudioCompressionSessionDelegate> delegate;

- (void)encodeSampleBuffer:(CMSampleBufferRef)sampleBuffer;
- (size_t)copyPCMSamplesIntoBuffer:(AudioBufferList *)ioData;

@end
