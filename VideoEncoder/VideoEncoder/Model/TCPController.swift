//
//  TCPController.swift
//  VideoEncoder
//
//  Created by Josip Cavar on 16/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

import UIKit

@objc protocol TCPControllerDelegate {
    
    func tcpController(controller: TCPController, didChangeConnectionNumber number: Int)
}

class TCPController: NSObject, NSStreamDelegate, SocketDelegate {
   
    let lockQueue = dispatch_queue_create("com.tcpController.LockQueue", nil)
    let socketQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    var socket: Socket?
    var connectionPool: [NSOutputStream] = []
    var dataBuffers: [UnsafeMutablePointer<UInt8>] = []
    var dataBuffersSizes: [Int] = []
    
    weak var delegate: TCPControllerDelegate?

    func startListening(port: Int32) {
      
        dispatch_async(socketQueue) {
            self.socket = Socket()
            self.socket?.delegate = self
            self.socket?.startListening(port, runLoop:NSRunLoop.currentRunLoop().getCFRunLoop())
            NSRunLoop.currentRunLoop().run()
        }
    }
    
    func stopListening() {
        
        self.socket?.delegate = nil
        self.socket?.stopListening()
    }
    
    func stopOutputStreams() {
        
        dispatch_sync(lockQueue) {
            for connection in self.connectionPool {
                self.tearDownOutputStream(connection)
            }
            self.connectionPool.removeAll(keepCapacity: false)
            self.dataBuffers.removeAll(keepCapacity: false)
            self.dataBuffersSizes.removeAll(keepCapacity: false)
        }
    }
    
    private func setupOutputStream(handle: CFSocketNativeHandle) -> NSOutputStream? {

        var writeStream: Unmanaged<CFWriteStream>?
        CFStreamCreatePairWithSocket(kCFAllocatorDefault, handle, nil, &writeStream)
        
        var outputStream: NSOutputStream? = writeStream?.takeRetainedValue()
        outputStream?.delegate = self

        dispatch_async(socketQueue) {
            outputStream?.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
            outputStream?.open()
            NSRunLoop.currentRunLoop().run()
        }
        return outputStream
    }
    
    private func tearDownOutputStream(outputStream: NSOutputStream) {
        
        dispatch_async(socketQueue) {
            outputStream.close()
            outputStream.removeFromRunLoop(NSRunLoop.mainRunLoop(), forMode: NSDefaultRunLoopMode)
            outputStream.delegate = nil
        }
    }
    
    func writeToOutputStream(data: UnsafePointer<UInt8>, dataSize: UInt) {

        dispatch_sync(lockQueue) {
            for i in 0 ..< self.connectionPool.count {
                var outputStream = self.connectionPool[i]
                var dataBufferSize = self.dataBuffersSizes[i]
                var dataSizeBigEndian = dataSize.bigEndian
                
                if (dataBufferSize == 0) {
                    self.dataBuffers[i] = UnsafeMutablePointer<UInt8>(malloc(dataBufferSize + 4 + dataSize))
                } else {
                    self.dataBuffers[i] = UnsafeMutablePointer<UInt8>(realloc(self.dataBuffers[i], dataBufferSize + 4 + dataSize))
                }
                // write packet size
                memcpy(self.dataBuffers[i].advancedBy(dataBufferSize), &dataSizeBigEndian, 4)
                // write payload
                memcpy(self.dataBuffers[i].advancedBy(dataBufferSize + 4), data, dataSize)
                self.dataBuffersSizes[i] = dataBufferSize + 4 + Int(dataSize)
                var status = outputStream.streamStatus
                var error = outputStream.streamError
                if dataBufferSize == 0 && status == NSStreamStatus.Open {
                    dispatch_async(self.socketQueue) {
                        self.writeToStream(outputStream)
                    }
                }
            }
        }
    }
    
    func writeToStream(stream: NSOutputStream) {
        
        dispatch_sync(lockQueue) {
            if let index = find(self.connectionPool, stream) {
                var length = min(512, self.dataBuffersSizes[index])
                if length > 0 {
                    var dataBuffer = self.dataBuffers[index]
                    var bytesWritten = stream.write(dataBuffer, maxLength: length)
                    if (bytesWritten > 0) {
                        self.dataBuffersSizes[index] = self.dataBuffersSizes[index] - bytesWritten
                        var newBuffer = UnsafeMutablePointer<UInt8>(malloc(UInt(self.dataBuffersSizes[index])))
                        memcpy(newBuffer, self.dataBuffers[index].advancedBy(bytesWritten), UInt(self.dataBuffersSizes[index]))
                        free(self.dataBuffers[index])
                        self.dataBuffers[index] = newBuffer
                    }
                }
            }
        }
    }
    
    
    // MARK NSStreamDelegate methods
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        
        switch eventCode {
        case NSStreamEvent.None:
            
            println("None")
            
        case NSStreamEvent.OpenCompleted:
            
            self.delegate?.tcpController(self, didChangeConnectionNumber: self.connectionPool.count)
            println("OpenCompleted")
            
        case NSStreamEvent.HasBytesAvailable:
            
            println("HasBytesAvailable")
            
        case NSStreamEvent.HasSpaceAvailable:
            
            if let stream = aStream as? NSOutputStream {
                writeToStream(stream)
            }
            
        case NSStreamEvent.ErrorOccurred:
            
            dispatch_sync(lockQueue) {
                if let index = find(self.connectionPool, aStream as NSOutputStream) {
                    var outputStream = self.connectionPool[index]
                    if outputStream.streamStatus == NSStreamStatus.Error {
                        self.tearDownOutputStream(outputStream)
                        self.connectionPool.removeAtIndex(index)
                        self.dataBuffers.removeAtIndex(index)
                        self.dataBuffersSizes.removeAtIndex(index)
                        self.delegate?.tcpController(self, didChangeConnectionNumber: self.connectionPool.count)
                        var error = outputStream.streamError
                        println("client disconnected errno: \(errno), error: \(error)")
                    }
                }
                println("ErrorOccurred")
            }
            
        case NSStreamEvent.EndEncountered:
            
            println("EndEncountered")
            
        default:
            
            println("default")
        }
    }
    
    
    // MARK SocketDelegate methods

    func socket(socket: Socket!, didConnectWithHandle handle: UnsafeMutablePointer<CFSocketNativeHandle>) {
        
        dispatch_sync(lockQueue, { () -> Void in
            var connection = self.setupOutputStream(handle.memory)
            if let connection = connection {
                println("client connected")
                self.connectionPool.append(connection)
                self.dataBuffers.append(UnsafeMutablePointer<UInt8>())
                self.dataBuffersSizes.append(0)
            }
        })
    }
}
