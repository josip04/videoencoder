//
//  CameraSessionController.swift
//  VideoEncoder
//
//  Created by Josip Cavar on 01/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

import UIKit
import AVFoundation

@objc protocol CameraSessionControllerDelegate {
    
    func cameraSessionDidOutputSampleVideoBuffer(sampleBuffer: CMSampleBuffer!)
    func cameraSessionDidOutputSampleAudioBuffer(sampleBuffer: CMSampleBuffer!)
}

class CameraSessionController: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {

    var session: AVCaptureSession
    var sessionQueue: dispatch_queue_t
    
    var videoDevice: AVCaptureDevice?
    var videoDeviceInput: AVCaptureDeviceInput?
    var videoDeviceOutput: AVCaptureVideoDataOutput?
    var stillImageOutput: AVCaptureStillImageOutput?
    
    var audioDevice: AVCaptureDevice?
    var audioDeviceInput: AVCaptureDeviceInput?
    var audioDeviceOutput: AVCaptureAudioDataOutput?
    
    weak var delegate: CameraSessionControllerDelegate?
   
    override init() {
        
        session = AVCaptureSession()
        session.sessionPreset = AVCaptureSessionPresetMedium;
        sessionQueue = dispatch_queue_create("CameraSessionControllerSession", DISPATCH_QUEUE_SERIAL)
        
        super.init();
        
        session.beginConfiguration()
        addVideoInput()
        addVideoOutput()
        addStillImageOutput()
        addAudioInput()
        addAudioOutput()
        session.commitConfiguration()
    }
    
    func startCamera() {
        
        dispatch_async(sessionQueue, {
            var weakSelf: CameraSessionController? = self
            self.session.startRunning()
        })
    }
    
    func stopCamera() {
        
        dispatch_async(sessionQueue, {
            self.session.stopRunning()
        })
    }
    
    func setVideoDeviceFormatWithZoomSupported() -> CGFloat {
        
        var zoom: CGFloat = videoDevice?.activeFormat.videoMaxZoomFactor ?? 1
        if zoom == 1 {
            videoDevice?.lockForConfiguration(nil)
            if let formats = videoDevice?.formats {
                for format in formats {
                    zoom = format.videoMaxZoomFactor
                    if zoom > 1 {
                        videoDevice?.activeFormat = format as? AVCaptureDeviceFormat
                        break
                    }
                }
            }
            videoDevice?.unlockForConfiguration()
        }
        return zoom
    }
    
    func addVideoInput() {
        
        var success: Bool = false
        videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        videoDeviceInput = AVCaptureDeviceInput.deviceInputWithDevice(videoDevice, error: nil) as? AVCaptureDeviceInput;
        if let videoDeviceInput = videoDeviceInput {
            session.addInput(videoDeviceInput)
        }
    }
    
    func addVideoOutput() {
        
        videoDeviceOutput = AVCaptureVideoDataOutput()
        if let videoDeviceOutput = videoDeviceOutput {
            videoDeviceOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey : Int(kCVPixelFormatType_32BGRA)]
            videoDeviceOutput.alwaysDiscardsLateVideoFrames = true
            videoDeviceOutput.setSampleBufferDelegate(self, queue: sessionQueue)
            session.addOutput(videoDeviceOutput)
        }
    }
    
    func addAudioInput() {
        
        var success: Bool = false
        audioDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeAudio)
        audioDeviceInput = AVCaptureDeviceInput.deviceInputWithDevice(audioDevice, error: nil) as? AVCaptureDeviceInput;
        if let audioDeviceInput = audioDeviceInput {
            session.addInput(audioDeviceInput)
        }
    }
    
    func addAudioOutput() {
        
        audioDeviceOutput = AVCaptureAudioDataOutput()
        if let audioDeviceOutput = audioDeviceOutput {
            audioDeviceOutput.setSampleBufferDelegate(self, queue: sessionQueue)
            session.addOutput(audioDeviceOutput)
        }
    }
    
    func addStillImageOutput() {
        
        stillImageOutput = AVCaptureStillImageOutput()
        if let stillImageOutput = stillImageOutput {
            stillImageOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
            session.addOutput(stillImageOutput)
        }
    }
    
    func setZoomLevel(zoomLevel: CGFloat) {
    
        videoDevice?.lockForConfiguration(nil)
        var maxZoom = videoDevice?.activeFormat.videoMaxZoomFactor
        if (zoomLevel < maxZoom) {
            videoDevice?.videoZoomFactor = zoomLevel
        }
        videoDevice?.unlockForConfiguration()
    }

    
    // MARK AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate methods
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        
        if captureOutput.isKindOfClass(AVCaptureVideoDataOutput.self) {
            delegate?.cameraSessionDidOutputSampleVideoBuffer(sampleBuffer)
        } else if captureOutput.isKindOfClass(AVCaptureAudioDataOutput.self) {
            delegate?.cameraSessionDidOutputSampleAudioBuffer(sampleBuffer)
        }
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didDropSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        
    }
}
