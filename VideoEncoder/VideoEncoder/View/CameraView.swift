//
//  CameraView.swift
//  VideoEncoder
//
//  Created by Josip Cavar on 01/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

import UIKit
import AVFoundation

class CameraView: UIView {

    override class func layerClass() -> AnyClass {
        
        return AVCaptureVideoPreviewLayer.self
    }

}
