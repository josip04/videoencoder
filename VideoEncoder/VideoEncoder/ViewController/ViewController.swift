//
//  ViewController.swift
//  VideoEncoder
//
//  Created by Josip Cavar on 01/10/14.
//  Copyright (c) 2014 Josip Cavar. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NSStreamDelegate {

    @IBOutlet weak var textFieldIPAddress: UITextField!
    @IBOutlet weak var textFielVideodPort: UITextField!
    @IBOutlet weak var textFieldAudioPort: UITextField!
    
    // MARK - view lifecycle methods

    override func viewDidLoad() {
        
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        
        textFieldIPAddress.text = Helper.getIFAddresses().last
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK - action methods
    
    @IBAction func buttonStartTouchUpInside(sender: AnyObject) {
        
        self.performSegueWithIdentifier("ShowCameraSegue", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        var cameraViewController: CameraViewController? = segue.destinationViewController as? CameraViewController
        if let cameraViewController = cameraViewController {
            cameraViewController.videoPort = (self.textFielVideodPort.text as NSString).intValue
            cameraViewController.audioPort = (self.textFieldAudioPort.text as NSString).intValue
        }
    }
}

